package shapes3d;

import shapes2d.Circle;

public class Cylinder extends Circle{
    double height;

    public Cylinder(int radius, double height) {
        super(radius);
        this.height = height;
    }
    public double volume(){
        return super.area() * height;
    }
    public  double area(){
        return 2* super.area()+(2*Math.PI*getradius())*height;
    }
    public String toString() {
        return "Radius:" + super.getradius() + "  Height:" + this.height;
    }

}


