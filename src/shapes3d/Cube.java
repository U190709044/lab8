package shapes3d;

import shapes2d.Square;

public class Cube extends Square {

    public Cube(int sidelenth) {
        super(sidelenth);
    }
    public double area(){
        return 6*super.area();
    }
    public double volume(){
        return Math.pow(super.getSidelenth(),3);
    }


    public String toString() {
        return "Side lenth:" + getSidelenth();
    }

}
