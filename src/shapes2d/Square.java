package shapes2d;

public class Square {
    int sidelenth;
    public Square(int sidelenth) {
        this.sidelenth = sidelenth;
    }
    public int getSidelenth(){
        return sidelenth;
    }
    public double area(){
        return sidelenth*sidelenth;
    }
    public String toString() {
        return "Sidelenth:" +this.sidelenth ;
    }
}
