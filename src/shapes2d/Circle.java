package shapes2d;

public class Circle {
    double radius;
    public Circle(int radius) {
        this.radius = radius;
    }
    public  double getradius(){
        return radius;
    }

    public double area(){
        return Math.PI*Math.pow(radius,2);
    }
    public String toString() {
        return "Radius:" + radius ;
    }





}

